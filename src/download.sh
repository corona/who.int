#!/bin/bash

[[ -z ${URL1} ]] && URL1=https://covid19.who.int/WHO-COVID-19-global-data.csv
[[ -z ${URL2} ]] && URL2=https://covid19.who.int/WHO-COVID-19-global-table-data.csv
[[ -z ${URL3} ]] && URL3=https://covid19.who.int/who-data/vaccination-data.csv
[[ -z ${URL4} ]] && URL4=https://covid19.who.int/who-data/vaccination-metadata.csv

[[ -z ${OUTPUT_DIR} ]] && OUTPUT_DIR=data/original

[[ -z ${OUT1} ]] && OUT1=${OUTPUT_DIR}/global-data.csv
[[ -z ${OUT2} ]] && OUT2=${OUTPUT_DIR}/global-table-data.csv
[[ -z ${OUT3} ]] && OUT3=${OUTPUT_DIR}/vaccination-data.csv
[[ -z ${OUT4} ]] && OUT4=${OUTPUT_DIR}/vaccination-metadata.csv

[[ -z ${OUT4_GZ} ]] && OUT4_GZ=${OUT4}.gz
[[ -z ${OUT3_GZ} ]] && OUT3_GZ=${OUT3}.gz

curl --create-dirs $URL1 -o ${OUT1}
curl --create-dirs $URL2 -o ${OUT2}
curl --create-dirs $URL3 -o ${OUT3_GZ}
curl --create-dirs $URL4 -o ${OUT4_GZ}

rm ${OUT3}
rm ${OUT4}

gzip -d ${OUT3_GZ}
gzip -d ${OUT4_GZ}

