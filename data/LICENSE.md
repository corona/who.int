Every file in the `data/` directory is licensed with [`CC BY-NC-SA 3.0 IGO`](https://creativecommons.org/licenses/by-nc-sa/3.0/igo/legalcode) by the upstream, WHO.

Previosly was the source code in https://codeberg.org/corona/who.int also licensed with this license, this was now changed to [Unlicense](https://unlicense.org/).